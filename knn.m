% Retorna a classe pertinente ao padr�o de acordo com os dados enviados.
% dados = conjunto de treianemto[n,x + 1]. Onde as linhas representam os n 
%         padroes(amostras) de treinamento e x representa o numero de
%         caracteristicas de um padrao. A ultima coluna representa a classe
%         do padr�o
% 
% padr�o = padrao que se deseja classificar contendo x elementos
% k      = quantidade de vizinhos que ser�o analisados
% distancia = 1 - distancia Euclidiana
%             2 - distancia de Manhattan
% prob = [1, qtdclasse + 1] = ultima coluna indica a classe com maior
% probabilidade da amostra
function prob = knn (dados, padrao, k, distancia, qtdclasses)

linhas = size(dados,1);
% remove a coluna relacionada a classe
caracteristicas = size(dados,2) - 1;
% uma coluna para as distancias e outra para as classes
distancias = zeros(linhas, 2); 
amostras = transpose(dados(1:linhas,1:caracteristicas));

switch distancia
    case 1 % distancia Euclidiana
        distancias(1:linhas, 1) = dist(padrao,amostras); 
    case 2 % distancia de Manhattan
        distancias(1:linhas, 1) = mandist(padrao,amostras); 
    otherwise % distancia Euclidiana
        distancias(1:linhas, 1) = dist(padrao,amostras); 
end
        
% preenche a coluna das classes
distancias(1:linhas, 2) = dados(1:linhas, caracteristicas + 1); 

ordenado = sortrows(distancias); 
% busca a classe mais frequente entre as k amostras com menor distancia
prob = zeros(1,qtdclasses + 1);
prob(1,qtdclasses + 1) = mode(ordenado(1:k, 2)); 

for i = 1:k
    prob(1,ordenado(i,2)) = prob(1,ordenado(i,2)) + 1;
end
prob(1, 1:qtdclasses) = prob(1, 1:qtdclasses) ./k;








