% dados = conjunto de treianemto[n,x + 1]. Onde as linhas representam os n 
%         padroes(amostras) de treinamento e x representa o numero de
%         caracteristicas de um padrao. A ultima coluna representa a classe
%         do padr�o
% kernel = 1 - Gaussiana
%          2 - triangular
% h      = janela do kernel
function fdp = parzen(dados,kernel, h, padrao)

% p = numero de caracteristicas das amostras
p = size(dados,2);
% n = numero de amostras
n = size(dados, 1);

soma = 0;

for i = 1:n

    switch kernel
        case 1 % Gaussiana calculando cada elemento 1 a 1
            v = (2*pi)^(-1/2);
            x = (padrao(1,1:p) - dados(i,1:p))./h(1,1:p);            
            resultado =  prod( exp(x.^2 .* (-0.5)) .* v);
        case 2 % triangular
             x = (padrao(1,1:p) - dados(i,1:p))./h(1,1:p); 
             for j = 1:p
                 if abs(x(1,j)) < 1
                    x(1,j) = 1 - abs(x(1,j));
                 else
                    x(1,j) = 0; 
                 end
             end
             resultado = prod(x);
        otherwise % Gaussiana multivariado
            v = (2*pi)^(-p/2);
            x = (padrao(1,1:p) - dados(i,1:p))./h(1,1:p);            
            resultado =  exp((x * x') .* (-0.5)) .* v;
    end
    soma = soma + resultado;
    
end
fdp = (1/(n*prod(h))) * soma;

