function [dists] = generate_distribution()

media1 = [0 4 0];
media2 = [0 3 3];
std1 = [4 4 0.25];
std2 = [1 1 0.25];
std12 = [1.7 -1.7 0];

dists = normalize_bivariate_distribution(media1, media2, std1, std2, std12);

end

function [normal_amostras] = normalize_bivariate_distribution(media1, media2, std1, std2, std12)
%Recebe os valores dados no projeto, n�o precisa tirar a ra�z
%Objects � a quantidade de objetos que ser�o gerados

% classe 1-1
mu1 = [media1(1) media2(1)];
sigma1 = [std1(1) std12(1); std12(1) std2(1)];

% classe 1-2
mu2 = [media1(2) media2(2)];
sigma2 = [std1(2) std12(2); std12(2) std2(2)];

% classe 2
mu3 = [media1(3) media2(3)];
sigma3 = [std1(3) std12(3); std12(3) std2(3)];

amostras = zeros(300,3);
normal_amostras = zeros(300,3);

% Gerando a distribui��o
dist = [mvnrnd(mu1, sigma1, 100); mvnrnd(mu2, sigma2, 100); mvnrnd(mu3, sigma3, 100)];
amostras(1:300, 1:2) = dist; 
amostras(1:200, 3) = 1; 
amostras(201:300, 3) = 2; 

% normaliza��o das distribui��es
%maxInDist = max2(amostras(1:300,1:2));
%minInDist = min2(amostras(1:300,1:2));

[rows,~] = size(amostras(1:300,1:2));
colMax = max(abs(amostras(1:300,1:2)),[],1);

normal_amostras = amostras(1:300,1:2)./repmat(colMax,rows,1);
normal_amostras(1:200, 3) = 1; 
normal_amostras(201:300, 3) = 2;


%normal_amostras = bsxfun(@rdivide,amostras(1:300,1:2),max(amostras(1:300,1:2))/2)-1;

% 
% file = fopen('amostrasFile.txt','w');
% 
% for i = 1:200
%     fprintf(file,'%.3f;%.3f;%.3f\r\n', normal_amostras(i,1),normal_amostras(i,2),normal_amostras(i,3));
% end
% 
% for i = 201:300
%     fprintf(file,'%.3f;%.3f;%.3f\r\n', normal_amostras(i,1),normal_amostras(i,2),normal_amostras(i,3));
% end
% 
% fclose(file);
% save 'amostras.mat' normal_amostras
end


