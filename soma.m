function classe = soma(posterioris,prioris)

%Recebe uma matriz chamada de posterioris de dimensao 3x3, onde cada linha
%corresponde aos dados de cada classificador (temos 3: parzen, KNN e EM) e
%cada coluna corresponde aos dados. A coluna 1 corresponde aos dados para a
%classe 1, i.e. na posicao (1,2) teremos a probabilidade de classificar a
%amostra como sendo da classe 1 dado pelo classificador 2 (nao importa a
%ordem que os classificadores estao nas linhas). A segunda coluna
%corresponde aos dados da classe 2 e a terceira coluna corresponde ao
%ground truth, i.e., a real classificacao de cada amostra.

%Recebe tambem um vetor chamado de prioris, de duas dimensoes, onde o
%indice 1 guarda a prob a priora da classe 1 e o indice 2 guarda a prob a
%priori da classe 2.

M = size(posterioris,1);

soma1 = 0;
soma2 = 0;

for i = 1:M
    soma1 = soma1 + posterioris(i,1);
    soma2 = soma2 + posterioris(i,2);
end

somaC1 = (1-M)*(prioris(1)) + soma1;
somaC2 = (1-M)*(prioris(2)) + soma2;

if somaC1 > somaC2
    classe = 1;
else
    classe = 2;
end