totalDados = 300;
load 'amostras.mat' normal_amostras
dados = normal_amostras;
classesPriori = [2/3 1/3];
classes = 2;

% = 200
totalDadosClasse1 = totalDados * classesPriori(1);
% = 70% treinamento = 210
totalDadosTreinamento = totalDados * 0.7;
% = 140 dados
divisaoTreinamento = totalDadosTreinamento * classesPriori(1);
%= 30% teste = 90
totalDadosTeste = totalDados * 0.3;
% = 60 dados
divisaoTeste = totalDadosTeste * classesPriori(1);
% = 270 dados
divisaoTreinamento2 = totalDados - totalDadosTeste * classesPriori(2);

treinamento(1:divisaoTreinamento,:) = dados(1:divisaoTreinamento, :);
teste(1:divisaoTeste,:) = dados(divisaoTreinamento+1:totalDadosClasse1, :);
treinamento(divisaoTreinamento + 1:totalDadosTreinamento,:) = dados(totalDadosClasse1 + 1:divisaoTreinamento2,:);
teste(divisaoTeste +1:totalDadosTeste, :) = dados(divisaoTreinamento2 + 1: totalDados, :);

fclassificadores = fopen('ResultadoClassifica��o.txt', 'w');

%*****************************************************************
% 2 Quest�o Letra A: Utilizando Expectation Maximization e Maximum
% Likelihood
%*****************************************************************

resultEM = EM(treinamento, divisaoTreinamento, classesPriori, teste);
[erros, matriz] = erros_e_matriz(resultEM(:, classes+2), resultEM(:, classes+1), divisaoTeste);

disp('Expectation Mazimization e Maximum Likelihood');
erros
matriz
fprintf(fclassificadores,'ExpecationMaximization-MaximunLikelihook; ErroGlobal:%.5f; ErroClasse1:%.5f; ErroClasse2:%.5f; VP:%d; VN:%d; FP:%d; FN:%d\r\n',erros(1),erros(2),erros(3),matriz(1),matriz(2),matriz(3),matriz(4));

%*****************************************************************
% 2 Quest�o Letra B: Janela Parzen
%*****************************************************************

errosParzen = zeros(1,10);
column = 0;

for h = 0.1:0.1:1
    column = column + 1;
    resultParzen = janela_parzen(treinamento, teste, h, [divisaoTreinamento, totalDadosTreinamento], classesPriori);
    errosParzenAux = taxaDeErro(teste(:, 3), resultParzen(:, 3), divisaoTeste);
    errosParzen(1, column) = errosParzenAux(1,1);
    matriz = corretudeMetodos(teste(:,3), resultParzen(:,3));
end

%bestH = 0;
minimumError = min(errosParzen);
for i = 1:10
    aux = errosParzen(1,i);
    if aux == minimumError
        bestH = i/10;
        %disp(bestH);
    end
end

resultParzen = janela_parzen(treinamento, teste, bestH, [divisaoTreinamento, totalDadosTreinamento], classesPriori);

disp('Janela de Parzen');
errosParzen = taxaDeErro(teste(:,3), resultParzen(:,3), divisaoTeste)
matriz = corretudeMetodos(teste(:,3), resultParzen(:,3))
disp('Melhor H');
disp(bestH);
fprintf(fclassificadores, 'Parzen; ErroGlobal:%.5f; ErroClasse1:%.5f; ErroClasse2:%.5f; VP:%d; VN:%d; FP:%d; FN:%d\r\n',errosParzen(1),errosParzen(2),errosParzen(3),matriz(1),matriz(2),matriz(3),matriz(4));
%*****************************************************************
% 2 Quest�o Letra C: Knn - Dist�ncia Euclidiana
%*****************************************************************

errosKnn = ones(1,classes + 1);

for k = 1:100
%dist�ncia euclidiana
resultKnn = classificador_knn(treinamento, teste, k, 1, classes);
[erros, matriz] = erros_e_matriz(resultKnn(:,classes+2), resultKnn(:,classes+1), divisaoTeste);

if (sum(errosKnn)/(classes+1) > sum(erros)/(classes+1))
    bestK = k;
    errosKnn = erros;
end


end
%resultKnn
disp('Melhor K = ');
disp(bestK);
errosKnn
matriz

fprintf(fclassificadores,'KNN; ErroGlobal:%.5f; ErroClasse1:%.5f; ErroClasse2:%.5f; VP:%d; VN:%d; FP:%d; FN:%d\r\n',errosKnn(1),errosKnn(2),errosKnn(3),matriz(1),matriz(2),matriz(3),matriz(4));


%*****************************************************************
% 2 Quest�o Letra D: MLP
%*****************************************************************

dadosTranspose = transpose(dados);
mlp = feedforwardnet(2);
inputs = [dadosTranspose(1:2, 1:150) dadosTranspose(1:2, 201:250)];
%targets = [dadosTranspose(3, 1:150) dadosTranspose(3, 201:250)];
targets = zeros(2,300);
targets(1, 1:200) = 1;
targets(2, 201:300) = 1;

tar = [targets(1:2, 1:150) targets(1:2, 201:250)];

mlp = configure(mlp, inputs, tar);

mlp.trainParam.showWindow = 0;
%mlp.trainParam.epochs = 500;
%mlp.trainParam.lr = 0.001;
mlp.divideFcn = 'dividerand';
mlp.divideMode = 'sample'; 
mlp.divideParam.trainRatio = 70/100;
mlp.divideParam.valRatio = 15/100;
mlp.divideParam.testRatio = 15/100;

[mlp, tr] = train(mlp, inputs, tar);

test = [dadosTranspose(1:2, 151:200) dadosTranspose(1:2, 251:300)];
output = sim(mlp, test);

%targetOut = zeros(2, 300);
%targetOut = 
%targetOut = [targets(1:2, 151:200) targets(1:2, 251:300)];
tar = [targets(1:2, 151:200) targets(1:2, 251:300)];

%targetsOut = [dadosTranspose(3, 151:200) dadosTranspose(3, 251:300)];

%erroMLP = taxaDeErro(tar, output, 50)

corretoClasse1 = 0;
corretoClasse2 = 0;
corretoGeral = 0;

for i = 1:100
    if(output(1,i) > output(2,i))
        output(1,i) = 1;
    else
        output(2,i) = 1;
    end
    
    if i < 51
        if(output(1,i) == tar(1,i))
            corretoGeral = corretoGeral + 1;
            corretoClasse1 = corretoClasse1 + 1;           
        end
    else
        if(output(2,i) == tar(2,i))
            corretoGeral = corretoGeral + 1;
            corretoClasse2 = corretoClasse2 + 1;
        end
    end
    
end

erroGeralMLP = 1 - (corretoGeral/100)
erroClasse1 = 1 - (corretoClasse1/50)
erroClasse2 = 1 - (corretoClasse2/50)

%output

%*****************************************************************
%2 Quest�o Letra E: Regra da Soma
%*****************************************************************

combinacao = zeros(3, classes);
soma = zeros(totalDadosTeste, 1);
resultSoma = zeros(totalDadosTeste,1);

for i = 1:totalDadosTeste
    combinacao(1,:) = resultEM(i,1:classes);
    combinacao(2,:) = resultParzen(i,1:classes);
    combinacao(3,:) = resultKnn(i,1:classes);
    
    resultSoma(i) = somaClassificadores(combinacao, classesPriori);
end

errorSoma = taxaDeErro(teste(:, 3), resultSoma(:, 1), divisaoTeste);
metodoSoma = corretudeMetodos(teste(:, 3), resultSoma(:, 1));

errorSoma
metodoSoma
fprintf(fclassificadores,'Regra Soma; ErroGlobal:%.5f; ErroClasse1:%.5f; ErroClasse2:%.5f; VP:%d; VN:%d; FP:%d; FN: %d\r\n',errorSoma(1),errorSoma(2),errorSoma(3),metodoSoma(1),metodoSoma(2),metodoSoma(3),metodoSoma(4));

%*********************************************************************
% 2 Quest�o Letra G e 3 Quest�o: Friedman test e Nemenyi teste
%*********************************************************************

kfold = 10;

dadosTreinamento = totalDados * (100 - kfold) / 100;
divisaoTreinamento = totalDadosTreinamento * classesPriori(1);
totalDadosTeste = totalDados * kfold/100;
divisaoTeste = totalDadosTeste * classesPriori(1);

erroExpectationMaximization = zeros(100,3);
matrizConfusaoExpectationMaximization = zeros(100,4);

erroParzen = zeros(100,3);
matrizConfusaoParzen = zeros(100,4);

erroKnn = zeros(100,3);
matrizConfusaoknn = zeros(100,4);

erroMLP = zeros(100,3);
matrizConfusaoMLP = zeros(100,4);

erroSoma = zeros(100,3);
matrizConfusaoSoma = zeros(100,4);

erroGeralMLP1 = zeros(100,1);
erroGeralMLP = 0;
corretoGeral = 0;

cont = 1;
for i = 1:10
   validacaoEstratificada =  estratificarKFold(dados, kfold);
   for j = 1:kfold
       teste = validacaoEstratificada(1:totalDadosTeste,:,j);
       treinamento = validacaoEstratificada((totalDadosTeste+1):totalDados,:,j);
       
       % *****************Expectation Maximization*************
       resultEM = EM(treinamento, divisaoTreinamento, classesPriori, teste);
       [mse, roc] = erros_e_matriz(resultEM(:,classes+2), resultEM(:,classes+1), divisaoTeste);
       erroExpectationMaximization(cont,:) = mse(1,:);
       matrizConfusaoExpectationMaximization(cont,:) = roc;
       %erroEM = taxaDeErro(teste(:,3), resultEM(:,3), divisaoTreinamento);
       %metodoEM = corretudeMetodos(teste(:,3), resultEM(:,3));
       %erroExpectationMaximization(cont,:) = metodoEM;
       %matrizConfusaoExpectationMaximization(cont,:) = metodoEM;
       % ******************************************************
       
       % ******************Janela de Parzen********************
%        erroGlobalParzen = zeros(1,10);
%        column = 0;
%        for h = 0.1:0.1:1
%           column = column + 1;
%           resultParzen = janela_parzen(treinamento, teste, h, [divisaoTreinamento dadosTreinamento], classesPriori);
%           erroParzenAux = taxaDeErro(teste(:,3), resultParzen(:,3), divisaoTreinamento);
%           erroGlobalParzen(1,column) = erroParzenAux(1,1);
%           metodoParzen = corretudeMetodos(teste(:,3), resultParzen(:,3));
%        end
%        
%        minimunError = min(erroGlobalParzen);
%        for i = 1:10
%           if erroGlobalParzen(1,i) == minimunError;
%               bestH = i/10;
%           end
%        end
%        
%        resultParzen = janela_parzen(treinamento, teste, bestH, [divisaoTreinamento dadosTreinamento], classesPriori);
%        errosParzen = taxaDeErro(teste(:,3), resultParzen(:,3), divisaoTreinamento);
%        metodoParzen = corretudeMetodos(teste(:,3), resultParzen(:,3));
%        erroParzen(cont,:) = errosParzen;
%        matrizConfusaoParzen(cont,:) = metodoParzen;
         
       resultParzen = janela_parzen(treinamento, teste, bestH, [divisaoTreinamento, totalDadosTreinamento], classesPriori);
       [mse, roc] = erros_e_matriz(resultParzen(:,classes+2), resultParzen(:,classes+1), divisaoTeste);
       erroParzen(cont,:) = mse;
       matrizConfusaoParzen(cont,:) = roc;
       % *******************************************************
       
       % ******************Knn**********************************
       resultKnn = classificador_knn(treinamento, teste, bestK, 1, classes);
       [mse, rooc] = erros_e_matriz(resultKnn(:,classes+2), resultKnn(:,classes+1),divisaoTeste);
       erroKnn(cont,:) = mse;
       matrizConfusaoknn(cont,:) = roc;
       % *******************************************************
       
       % ******************MLP**********************************
       mlp = feedforwardnet(5);
       treinamentoMLP = treinamento;
       treinamentoMLP = transpose(treinamentoMLP);
       testeMLP = teste;
       testeMLP = transpose(testeMLP);
       %inputs = [dadosTranspose(1:2, 1:150) dadosTranspose(1:2, 201:250)];
       inputs = treinamentoMLP(1:2,:);
       targets = treinamentoMLP(3,:);
       %targets = zeros(2, 300);
       %targets(1, 1:200) = 1;
       %targets(2, 201:300) = 1;

       %tar = [targets(1:2, 1:150) targets(1:2, 201:250)];

       mlp = configure(mlp, inputs, targets);

       mlp.trainParam.showWindow = 0;
       %mlp.trainParam.epochs = 500;
       %mlp.trainParam.lr = 0.001;
       mlp.divideFcn = 'dividerand';
       mlp.divideMode = 'sample'; 
       %mlp.divideParam.trainRatio = 70/100;
       %mlp.divideParam.valRatio = 15/100;
       %mlp.divideParam.testRatio = 15/100;

       [mlp, tr] = train(mlp, inputs, targets);
       
       test = testeMLP(1:2,:);
       %test = [dadosTranspose(1:2, 151:200) dadosTranspose(1:2, 251:300)];
       output = sim(mlp, test);

       %targetOut = zeros(2, 300);
       %targetOut = [targets(1:2, 151:200) targets(1:2, 251:300)];
       tar = testeMLP(3,:);
       
       corretoGeral = 0;
       
       for l = 1:30
           if(output(1,l) < 2)    
               output(1,l) = 1;
           else
               output(1,l) = 2;
           end
    
           if l < 21
               if(output(1,l) == tar(1,l))
                   corretoGeral = corretoGeral + 1;
                   corretoClasse1 = corretoClasse1 + 1;           
               end
           else
               if(output(1,l) == tar(1,l))
                   corretoGeral = corretoGeral + 1;
                   corretoClasse2 = corretoClasse2 + 1;
               end
           end
       end

       aux = 1 - (corretoGeral/30);
       erroGeralMLP = erroGeralMLP + (1 - (corretoGeral/30));
       erroGeralMLP1(cont,1) = aux;
       aux = 0;
       
       %targetsOut = [dados(3, 151:200) dados(3, 251:300)];

       %plotconfusion(tar, output);

       %output
       % *******************************************************
       
       % ******************Soma*********************************
       
       combinacao = zeros(3, classes);
       resultSoma = zeros(totalDadosTeste, 1);
       
       for l = 1:totalDadosTeste
          combinacao(1,:) = resultEM(l,1:classes);
          combinacao(2,:) = resultParzen(l,1:classes);
          combinacao(3,:) = resultKnn(l,1:classes);
          
          resultSoma(l) = somaClassificadores(combinacao, classesPriori);
          
       end
       [mse, roc] = erros_e_matriz(teste(:,classes+1), resultSoma, divisaoTeste);
       erroSoma(cont,:) = mse;
       matrizConfusaoSoma(cont,:) = roc;
       
       cont = cont + 1;
   end
end

disp('Erro Expectation Maximization');
medio = sum(erroExpectationMaximization,1)./100

disp('Erro Parzen');
medio = sum(erroParzen,1)./100

disp('Erro Knn');
medio = sum(erroKnn,1)./100

disp('Erro Soma');
medio = sum(erroSoma,1)./100

disp('Erro MLP');
medio = erroGeralMLP/100

% ******************Friedman Test**************************

Allalgorithms = [erroExpectationMaximization(:,1), erroParzen(:,1), erroKnn(:,1), erroSoma(:,1), erroGeralMLP1(:,1)];
[p, table, stats] = friedman(Allalgorithms, 1);

[c, m, h, nmas] = multcompare(stats)



