function amostras_questao1 = criar_amostras()
amostras = zeros(300,3);

% gerando 150 amostras da classe 1
MU1 = [0 0];
SIGMA1 = [4 1.7; 1.7 1];
amostras(1:150,1:2) = mvnrnd(MU1,SIGMA1,150);
amostras(1:150,3) = 1;

% gerando 150 amostras da classe 2 (100 com m�dia MU2 e vari�ncia SIGMA2 e
% 50 com m�dia MU3 e vari�ncia SIGMA3)
MU2 = [0 3];
SIGMA2 = [0.25 0; 0 0.25];

MU3 = [4 3];
SIGMA3 = [4 -1.7; -1.7 1];

amostras(151:300,1:2) = [mvnrnd(MU2,SIGMA2,100);mvnrnd(MU3,SIGMA3,50)];
amostras(151:300,3) = 2;

arquivoAmostras = fopen('amostras.txt','w' );
for i = 1:300
fprintf(arquivoAmostras,'%.3f;%.3f;%.0f\r\n', amostras(i,1),amostras(i,2), amostras(i,3));
end
fclose(arquivoAmostras);
save 'amostras.mat' amostras