% dados[n,p] contem as amostras onde n � o numero de amostras e p eh o
% numero de caracteristicas
% g = numero de gaussianas que se deseja encontrar nas amostras
function [priori,medias,covariancia] = expectation_maximization(dados,g)

n = size(dados,1);
p = size(dados,2);

% inicializa os valores a priori, as medias e as covariancias para as g
% gaussianas utilizando o metodo k-means

[classificacao,medias] = kmeans(dados,g); 

% separa os dados de acordo com os centros
% qtd = quantidade de amostras em cada centro
% amostras = amostras separadas por centro
qtd = zeros(1,g);
amostras = zeros(n,p,g);

for i = 1:n
    qtd(classificacao(i)) = qtd(classificacao(i)) + 1; 
    amostras(qtd(classificacao(i)), : , classificacao(i)) = dados(i,:);
end

priori = qtd ./n;

covariancia = zeros(p,p,g);
for i = 1:g
    covariancia(:,:,i) = cov(amostras(1:qtd(i),:,i));
end

% Calcula o log da verossimilhanca

log1 = log_verossimilhanca(dados,g,priori,medias,covariancia); 
criterio = 0.1; 
qtd_iteracoes = 1000; 

for idx = 1:qtd_iteracoes
    % step E

    E = zeros(n,g);
    a = (2*pi)^(0.5*p);
    for i=1:n    
        for j=1:g
            determinante = sqrt(det(covariancia(:,:,j)));
            inversa = inv(covariancia(:,:,j));
            x = dados(i,:) - medias(j,:);
            fdp = exp(-0.5*x*inversa*x')/(a*determinante);
            %fdp = exp(-0.5*x/inv(covariancia(:,:,j))*x')/(a*determinante);
            E(i,j) = priori(j)*fdp;
        end
        E(i,:) = E(i,:)/sum(E(i,:));
    end

    % step M
    priori = zeros(1,g); 
    medias = zeros(g,p);
    covariancia = zeros(p,p,g);
    for i=1:g,  
        for j=1:n,
            priori(i) = priori(i) + E(j,i);
            medias(i,:) = medias(i,:) + E(j,i)*dados(j,:);
        end
        medias(i,:) = medias(i,:)/priori(i);
    end
    for i=1:g
        for j=1:n
            x = dados(j,:)- medias(i,:);
            covariancia(:,:,i) = covariancia(:,:,i) + E(j,i)*(x'*x);
        end
        covariancia(:,:,i) = covariancia(:,:,i)/priori(i);
    end
    priori = priori./n;
   
    %calcula o log
    log2 = log_verossimilhanca(dados,g,priori,medias,covariancia);
    % checa criterio de convergencia
    if (abs(100*(log2-log1)/log1)<criterio)
        break;
    else
        log1 = log2;
    end
end 


function l = log_verossimilhanca(dados,g,priori,medias,covariancia)
n = size(dados,1);
media_geral = mean(dados);
cov_geral = cov(dados);
l = 0;
for i=1:g
    inversa = inv(covariancia(:,:,i));
    l = l + priori(i)*(-0.5*n*log(det(2*pi*covariancia(:,:,i))) ...
        -0.5*(n-1)*(trace(inversa*cov_geral)+(media_geral-medias(i,:))*inversa*(media_geral-medias(i,:))'));
end


