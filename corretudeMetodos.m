%================C�lculo dos valores da curva de roc====================%
%ClassificacaoOriginal = conjunto de amostras de treinamento 
%resultadoMetodo = conjunto de amostras de teste
%Defini��o do tp, tn, fp, fn

function corretude = corretudeMetodos(classificacaoOriginal,resultadoMetodo)

verdadeiropositivo = 0;
verdadeironegativo = 0;
falsopositivo = 0;
falsonegativo = 0;

n = size(classificacaoOriginal,1);

    for i = 1:n
        if (classificacaoOriginal(i,1) == resultadoMetodo(i,1))
            if(classificacaoOriginal(i,1) == 1)
                verdadeiropositivo = verdadeiropositivo+1;
            else
                verdadeironegativo = verdadeironegativo+1;
            end
        else
            if(classificacaoOriginal(i,1) == 1)
                falsopositivo = falsopositivo + 1;
            else
                falsonegativo = falsonegativo + 1;
            end
        end
    end
    
corretude = [verdadeiropositivo,verdadeironegativo,falsopositivo,falsonegativo];