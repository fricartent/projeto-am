%=============Gera��o dos grupos de amostras estratificados=========%

function resultado = validacaoCruzada10FoldEstratificada(dados, kFold)
%dados = toda a base de amostras gerada na primeira quest�o 
%kFold = n� de folds definido (10)

%Vetores de apoio para a gera��o da matriz estratificada
sorteadosC11 = zeros(1,100);
sorteadosC12 = zeros(1,100);
sorteadosC2 = zeros(1,100);

linhas = size(dados,1);
colunas = size(dados,2);
tamanhaFold = linhas/kFold;

%Matriz de dados estratificada
estratificado = zeros(linhas,colunas);

%In�cio da gera��o de matriz estratificada
for i = 1:kFold 

    %Folds de 30 elementos, pois ser�o 10 folds definidos pela divis�o das
    %300 amostras por 10 grupos. 
    limiteFold = ((i-1)*(linhas/kFold))+1;
    
    %Controlador da remo��o de uma amostra para inser��o em matriz
    %estratificada
    flag = 1; 

    for j = limiteFold:(i*(linhas/kFold))
        
        %Capturando amostra da classe 1 para inser��o na matriz
        %estratificada 
        if (flag == 1) 
            posicaoAmostra1 = 0;  %Posi��o do elemento capturado  
            presente = 1; %Flag que sinaliza se a amostra da posi��o j� se localiza na matriz estratificada
            flag = 2; %Autoriza��o do proximo elemento a ser capturado ser da classe 2 
            while (posicaoAmostra1==0 || presente == 1)
                posicaoAmostra1 = randi(100); 
                if (sorteadosC11(posicaoAmostra1) == 0)
                    sorteadosC11(posicaoAmostra1) = 1;
                    presente = 0;
                else
                    presente = 1;
                end
            end            
            estratificado(j,:) = dados(posicaoAmostra1,:);        
       
        elseif (flag == 2)
            posicaoAmostra2 = 0;
            presente = 1; 
            flag = 3;
            while (posicaoAmostra2==0 || presente == 1)
                posicaoAmostra2 = randi(100);
                if (sorteadosC12(posicaoAmostra2) == 0)
                    sorteadosC12(posicaoAmostra2) = 1;
                    presente = 0;
                else
                    presente = 1;
                end

            end
            estratificado(j,:) = dados((posicaoAmostra2+100),:);
            
        else
            posicaoAmostra = 0;
            presente = 1;
            flag = 1;
            while (posicaoAmostra==0 || presente == 1)
                posicaoAmostra = randi(100);
                if (sorteadosC2(posicaoAmostra) == 0)
                    sorteadosC2(posicaoAmostra) = 1;
                    presente = 0;
                else
                    presente = 1;
                end

            end
            estratificado(j,:) = dados((posicaoAmostra+200),:);
        end
        
    end    
end
%Fim da gera��o de matriz estratificada

%In�cio da ordena��o dos folds 
tam = tamanhaFold - 1;
for i = 1:kFold
    limiteFold = ((i-1)*(linhas/kFold))+1;
    estratificado(limiteFold:limiteFold+tam,:) = sortrows(estratificado(limiteFold:limiteFold+tam,:),3);
end
%Fim da ordena��o dos folds 

%In�cio da ordena��o dos folds da valida��o cruzada (teste e treinamentos) 
resultado = zeros(linhas,colunas,kFold);

for i = 1:kFold
    %Atribui��o dos dados de teste
    limiteFold = ((i-1)*(linhas/kFold))+1;
    resultado(1:tamanhaFold,:,i) = estratificado(limiteFold:limiteFold+tam,:);
    
    %Atribui��o 9 folds de treinamento 
    if limiteFold == 1
        resultado(tamanhaFold+1:linhas,:,i) = estratificado((limiteFold+tamanhaFold):linhas,:);
    elseif (limiteFold == (linhas-tam))
        resultado(tamanhaFold+1:linhas,:,i) = estratificado(1:(linhas-tamanhaFold),:);  
    else
        tmp = tamanhaFold+(limiteFold-1);
        resultado((tamanhaFold+1):tmp,:,i) = estratificado(1:(limiteFold-1),:);
        resultado((tmp+1):linhas,:,i) = estratificado((limiteFold+tamanhaFold):linhas,:);
    end
end

for i = 1:kFold
    resultado(tamanhaFold+1:linhas,:,i) = sortrows(resultado(tamanhaFold+1:linhas,:,i),3);
end

%Fim da ordena��o dos folds da valida��o cruzada (teste e treinamentos)

end 

