function classification = EM(train, divisor, priori, test)
% divisor = � a �ltima posi��o do padr�o de entrada da classe 1
% classification [n, classes + 2] = matriz que cont�m a classifica��o
% a posteriori de cada amostra.
% n � a quantidade de amostras no conjunto de teste
% classes � a quantidade de classes
% + 1 coluna para o resultado da classifica��o a posteriori
% + 1 coluna para o valor real de classifica��o localizado na base
% de teste

classes = 2;
%n�mero de amostras do conjunto de treinamento e teste
size_train = size(train,1);
size_test = size(test,1);
%n�mero de caracter�sticas de cada amostra
number_sample = size(test,2) - 1;

classification = zeros(size_test, classes + 2);
classification(1:size_test, classes + 2) = test(1:size_test, number_sample + 1);

%
[priori1,medias1,variancias1] = expectation_maximization(train(1:divisor,1:number_sample),2);

%densidades da normal multivariada
class11 = mvnpdf(test(:,1:number_sample),medias1(1,1:number_sample), variancias1(:,:,1));
class12 = mvnpdf(test(:,1:number_sample), medias1(2,1:number_sample), variancias1(:,:,2));

density_class1 = (class11 .* priori1(1,1)) + (class12 .* priori1(1,2));

%m�dia e variancia das amostras da classe 2
%densidade das amostras da classe 2
mean_class2 = mean(train((divisor + 1):size_train,1:number_sample));
variance_class2 = cov(train((divisor + 1):size_train, 1:number_sample));

density_class2 = mvnpdf(test(:,1:number_sample), mean_class2, variance_class2);

% prob a posteriori de todas as amostras
% prob a posteriori = densidade * priori

for i = 1:size_test
    classification(i,1) = density_class1(i,1) * priori(1,1) / (density_class1(i,1) * priori(1,1) + density_class2(i,1) * priori(1,2));
    classification(i,2) = density_class2(i,1) * priori(1,2) / (density_class1(i,1) * priori(1,1) + density_class2(i,1) * priori(1,2));
    
    if(classification(i,1) > classification(i,2))
        classification(i, classes + 1) = 1;
    else
        classification(i, classes + 1) = 2;
    end
    
end

end


function [priori, means, covariance] = expectation_maximization(data, number_gaussian)

n = size(data,1);
p = size(data,2);

%inicializa os valores a priori. As medias e covariancias utilizando 
% o kmeans
[classificacao, means] = kmeans(data,number_gaussian);

qtd = zeros(1,number_gaussian);
samples = zeros(n,p,number_gaussian);

for i = 1:n
    qtd(classificacao(i)) = qtd(classificacao(i)) + 1;
    samples(qtd(classificacao(i)), : , classificacao(i)) = data(i,:);
end

priori = qtd ./n;
covariance = zeros(p,p,number_gaussian);

for i = 1:number_gaussian
    covariance(:,:,i) = cov(samples(1:qtd(i),:,i));
end

log1 = log_likelihood(data,number_gaussian,priori,means,covariance);
criterio = 0.1;
iteration = 1000;

for index = 1:iteration
    
    expectation = zeros(n,number_gaussian);
    a = (2*pi)^(0.5*p);
    
    %expectation
    for i = 1:n
        for j = 1:number_gaussian
            determinante = sqrt(det(covariance(:,:,j)));
            inverse = inv(covariance(:,:,j));
            x = data(i,:) - means(j,:);
            fdp = exp(-0.5*x*inverse*x')/(a*determinante);
            expectation(i,j) = priori(j)*fdp;
        end
        expectation(i,:) = expectation(i,:)/sum(expectation(i,:));
    end
    
   %maximization
   priori = zeros(1,number_gaussian);
   means = zeros(number_gaussian,p);
   covariance = zeros(p,p,number_gaussian);
   
   for i = 1:number_gaussian
       for j =1:n
           priori(i) = priori(i) + expectation(j,i);
           means(i,:) = means(i,:) + expectation(j,i) * data(j,:);
       end
       means(i,:) = means(i,:)/priori(i);
   end
   
   for i = 1:number_gaussian
       for j = 1:n
           x = data(j,:) - means(i,:);
           covariance(:,:,i) = covariance(:,:,i) + expectation(j,i)*(x'*x);
       end
       covariance(:,:,i) = covariance(:,:,i)/priori(i);
   end
   
   priori = priori./n;
   
   log2 = log_likelihood(data,number_gaussian,priori,means,covariance);
   
   %criterio de convergencia
   if(abs(100*(log2 - log1)/log1)<criterio)
       break;
   else
       log1 = log2;
   end
   
end

end

function l = log_likelihood(dados,g,priori,medias,covariancia)
n = size(dados,1);
media_geral = mean(dados);
cov_geral = cov(dados);
l = 0;
for i=1:g
    inversa = inv(covariancia(:,:,i));
    l = l + priori(i)*(-0.5*n*log(det(2*pi*covariancia(:,:,i))) ...
        -0.5*(n-1)*(trace(inversa*cov_geral)+(media_geral-medias(i,:))*inversa*(media_geral-medias(i,:))'));
end
end

function [me, covariance] = maximun_likelihood(data)
me = mean(data); covariance = cov(data);
end
