% classificacao [n,c + 2]
% onde n = quantidade de amostras de teste
%      c = quantidade de classes + 1 coluna para o resultado da
%      classificação + 1 coluna para o valor que deveria ser classificado
function classificacao = classificador_knn(treinamento,teste, k, distancia,c)

nt = size(teste,1);
p = size(teste,2) - 1;
classificacao = zeros(nt,c + 2);

classificacao(1:nt,c + 2) = teste(1:nt, p + 1);

for i = 1:nt
    classificacao(i, 1: c+ 1) = knn(treinamento, teste(i,1:p),k, distancia, c);    
end