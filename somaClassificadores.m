%========Combina��o dos classificadores EM, kNN e Parzen pelo m�todo da
%soma========%
% Regra de decis�o da combina��o pelo m�todo da soma 
% (1-L)p(wj) + somat�rio(posterioris da classe 1) > (1-L)p(wk) + somat�rio(posterioris da classe 2) 
%
% p(w) = priori de ser das classes 1 ou 2 (1/2, 1/2) 
% posterioriClassificadores = matriz com as classes atribuidas pelos
% classificadores (EM, PARZEN e kNN) a cada amostra 

function classe = somaClassificadores(posterioriClassificadores,priori)

L = size(posterioriClassificadores,1);

somaClasse1 = 0;
somaClasse2 = 0;

for i = 1:L
    somaClasse1 = somaClasse1 + posterioriClassificadores(i,1);
    somaClasse2 = somaClasse2 + posterioriClassificadores(i,2);
end

somaC1 = (1-L)*(priori(1)) + somaClasse1;
somaC2 = (1-L)*(priori(2)) + somaClasse2;

if somaC1 > somaC2
    classe = 1;
else
    classe = 2;
end

%========Combina��o dos classificadores EM, kNN e Parzen pelo m�todo da
%soma========%