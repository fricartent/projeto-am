function rand = rand_index(treinamento, fuzzy_hard)

n = size(treinamento, 1);
classe = size(treinamento,2);
hard = size(fuzzy_hard,2);

a = 0;
b = 0;
c = 0;
d = 0;
%Calculo do a
for i = 1:n-1
    for j = i +1 : n
        if(fuzzy_hard(i,hard) == fuzzy_hard(j,hard))
            mesmogrupo = 1;
        else
            mesmogrupo = 0;
        end
        if(treinamento(i,classe) == treinamento(j,classe))
            mesmaclasse = 1;
        else
            mesmaclasse = 0;
        end
        
        a = a + mesmogrupo * mesmaclasse;
        b = b + (1 - mesmogrupo) * mesmaclasse;
        c = c + mesmogrupo * (1 - mesmaclasse);
        d = d + (1- mesmogrupo)* (1- mesmaclasse);
    end
end

m = n *(n-1) / 2;
rand_numerador = (a + d) - ((a+b) * (a + c) + (b+ d) * (c + d))/m;
rand_denominador = m - ((a+b)* (a+c) + (b+d) * (c+d))/m;
rand = rand_numerador/rand_denominador;