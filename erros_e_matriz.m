% e = [mse global, mse classe1, mse classe2]
% r = [vp,vn,fp,fn] classe 1 = verdadeiro; classe 2 = falso
% classificados/observados [n,1] = contem os dados que ser�o comparados considera-se
% que p vetor contem apenas 1s e 2s
% div = posicao final dos dados da classe 1
function [e, r] = erros_e_matriz(classificados,observados, div)

n = size(classificados,1);
mseglobal = sum((classificados - observados).^2)/n;

mseclasse1 = sum((classificados(1:div,1) - observados(1:div,1)).^2)/div;
mseclasse2 =sum((classificados(div + 1:n,1) - observados(div + 1:n,1)).^2)/(n -div);

e = [mseglobal, mseclasse1, mseclasse2];
vp = 0;
vn = 0;
fp = 0;
fn = 0;

for i = 1:n
    if (classificados(i,1) == observados(i,1))
        if(classificados(i,1) == 1)
            vp = vp+1;
        else
            vn = vn+1;
        end
    else
        if(classificados(i,1) == 1)
            fp = fp + 1;
        else
            fn = fn + 1;
        end
    end
end
r = [vp,vn,fp,fn];