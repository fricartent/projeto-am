function estratificado_final = estratificarKFold(amostras,k)
%function estratificado = estratificarKFold
%amostras = criar_amostras();

M = size(amostras,1);
N = size(amostras,2);

%M eh o numero de linhas, N o numero de colunas

estratificado = zeros(M,N);

%k = 10; %numero de folds
%Talvez passar o numero de folds na chamada da funcao
sorteadosC11 = zeros(1,100);
sorteadosC12 = zeros(1,100);
sorteadosC2 = zeros(1,100);

for i = 1:k %cria cada um dos folds

    ii = ((i-1)*(M/k))+1;%diz o ponto na matriz estratificada onde comeca cada fold
    controle = 1; %diz de que classe vamos retirar uma amostra para inserir na matriz estratificada

    for j = ii:(i*(M/k))%no caso do 10-fold, sao 30 iteracoes pois M = 300 e k = 10, cada fold tem 30 amostras
        if (controle == 1) %colhemos da classe 1-1
            j11 = 0;    
            verif = 1; %controle para saber se a amostra jah foi usada
            while (j11==0 || verif == 1)
                j11 = randi(100);
                if (sorteadosC11(j11) == 0)
                    sorteadosC11(j11) = 1;
                    verif = 0;
                else
                    verif = 1;
                end

            end
            estratificado(j,:) = amostras(j11,:);
            controle = 2; %passamos o token para a seguinte classe
        %---------------------------------------------------------
        %---------------colhemos da classe 1-2--------------------
        %---------------------------------------------------------
        elseif (controle == 2)
            j12 = 0;
            verif = 1; %controle para saber se a amostra jah foi usada
            while (j12==0 || verif == 1)
                j12 = randi(100);
                if (sorteadosC12(j12) == 0)
                    sorteadosC12(j12) = 1;
                    verif = 0;
                else
                    verif = 1;
                end

            end
            estratificado(j,:) = amostras((j12+100),:);
            controle = 3;%passamos o token para a seguinte classe
        %---------------------------------------------------------
        %----------------colhemos da classe 2---------------------
        %---------------------------------------------------------
        else
            j2 = 0;
            verif = 1; %controle para saber se a amostra jah foi usada
            while (j2==0 || verif == 1)
                j2 = randi(100);
                if (sorteadosC2(j2) == 0)
                    sorteadosC2(j2) = 1;
                    verif = 0;
                else
                    verif = 1;
                end

            end
            estratificado(j,:) = amostras((j2+200),:);
            controle = 1; %passamos o token para a seguinte classe
        end
        
    end    
end
tamanho = M/k;%para k = 10 e M = 300, tamanho = 30
tam = tamanho - 1;%levando em conta o mesmo caso comentado acima, tam = 29

for i = 1:k
    ii = ((i-1)*(M/k))+1;
    estratificado(ii:ii+tam,:) = sortrows(estratificado(ii:ii+tam,:),3);
end
ii = 1;
estratificado_final = zeros(M,N,k);

for i = 1:k
    ii = ((i-1)*(M/k))+1;
    %recebe o teste
    estratificado_final(1:tamanho,:,i) = estratificado(ii:ii+tam,:);
    
    %recebe o treinamento
    if ii == 1
        estratificado_final(tamanho+1:M,:,i) = estratificado((ii+tamanho):M,:);
    elseif (ii == (M-tam))
        estratificado_final(tamanho+1:M,:,i) = estratificado(1:(M-tamanho),:);  
    else
        %ate onde se colhe o treinamento
        tmp = tamanho+(ii-1);
        estratificado_final((tamanho+1):tmp,:,i) = estratificado(1:(ii-1),:);
        %a partir do final de onde se colhe o treinamento at? o fim
        estratificado_final((tmp+1):M,:,i) = estratificado((ii+tamanho):M,:);
    end
end
for i = 1:k
    %estratificado_final = sortrows(estratificado_final(1:tamanho,:,i),3);
    estratificado_final(tamanho+1:M,:,i) = sortrows(estratificado_final(tamanho+1:M,:,i),3);
end


