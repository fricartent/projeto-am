%====================CLASSIFICADOR PARZEN========================%
% h = par�metro de suaviza��o 
% div = divisor das classes de amostras no conjunto de treinamento. 

function classificacao = janela_parzen(treinamento, teste, h, div, priori)
%numero de amostras no conjunto de teste = 90
nt = size(teste,1);
%numero de caracteristicas em uma amostra = 2
p = size(teste,2) - 1;

% numero de classes = 2
c = 2;

%matriz com os valores resultante da classifica��o das amostras 
%n� de linhas = nt
%n� de colunas = c + 2 (qtd de classes de amostras + 1 coluna para o
%resultado da classifica��o + 1 para o valor que deveria ser classificado) 
classificacao = zeros(nt,c + 2);

%preenchendo a coluna do valor que deveria ser classificado 
classificacao(1:nt,c+ 2) = teste(1:nt, p + 1);

%matriz com as densidades de probabilidade 
densidade = zeros(nt,c);

%C�lculo da fdp das amostras
for i = 1:nt
    for j = 1:c
        if(j == 1)
            
            densidade(i,j) = parzen(treinamento(1:div(1,j),1:p),h,teste(i,1:p));
        
        else
            densidade(i,j) = parzen(treinamento(div(1,j-1)+1:div(1,j),1:p),h,teste(i,1:p));
        end
    end
    classe = 1;
    
    for j = 1:c
       classificacao(i,j) = (densidade(i,j) * priori(1,j))/ sum(densidade(i,:) .* priori);
       if classificacao(i,classe) < classificacao(i,j)
           classe = j;
       end
    end
    classificacao(i,c + 1) = classe;
end
%====================CLASSIFICADOR PARZEN========================%

%==========C�LCULO DA DENSIDADE DAS AMOSTRAS============% 

function densidade = parzen(treinamento, h, teste)
% p = numero de caracteristicas das amostras
p = size(treinamento,2);
% n = numero de amostras
n = size(treinamento, 1);

% Calcula o V (volume) dos dados. 
v = (2*p)^(-1/2);

% Fun��o de Kernel 
% Estimador probabil�stico n�o param�trico (sem m�dia e desvio padr�o)
% kernel = x - xi / h 
somatorio = 0;

for i = 1:n

    u = (teste(1,1:p) - treinamento(i,1:p))./h;            
    resultado =  prod( exp(u.^2 .* (-0.5)) .* v);
    
    somatorio = somatorio + resultado;        
end

%F�rmula da estimativa da densidade do ponto x na regi�o R
%p(x) = 1/n * Somat�rio((1/h) * kernel))
densidade = (1/(n*prod(h))) * somatorio;

%==========C�LCULO DA DENSIDADE DAS AMOSTRAS============%
