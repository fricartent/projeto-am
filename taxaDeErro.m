%==============C�lculo da taxa de erro ====================%
function taxaErro = taxaDeErro(classificacaoOriginal, resultadoMetodo, divisorClasses)
%ClassificacaoOriginal = base de treinamento 
%resultadoMetodo = base de teste em an�lise
%divisorClasses = posi��o que divide as amostra da classe 1 da 2

baseTreinamento = size(classificacaoOriginal,1);

erroGeral = sum((classificacaoOriginal - resultadoMetodo).^2)/baseTreinamento;

diferenca = (classificacaoOriginal(1:divisorClasses,1) - resultadoMetodo(1:divisorClasses,1)); 
erroclasse1 = sum(diferenca.^2)/divisorClasses;
erroclasse2 =sum((classificacaoOriginal(divisorClasses + 1:baseTreinamento,1) - resultadoMetodo(divisorClasses + 1:baseTreinamento,1)).^2)/(baseTreinamento - divisorClasses);

taxaErro = [erroGeral, erroclasse1, erroclasse2];
