%Fun��o principal
%Faz o c�lculo da fun��o objetivo at� o crit�rio de parada
function [diff,centroids,U,Y] = VKFCM_K_LP(matrix, Y,m,t,erro)
%m = 2;

%matrix = load_distribution();
U = generate_MatrixU();
centroids = find_initial_centroids();
%K = gaussianKernel(matrix);
% s = vkfcm_k_lp(matrix, centroids, U, lambda,m);
% 
% Y = update_lambda_matrix(m,matrix,centroids,U);
% 
% U = update_matrix_fuzzy_degree(m,matrix, centroids);

cont = 1;
J_t0 = kernel_fuzzy(matrix,centroids,U,Y,m);
J_t1 = 0;

diff = abs(J_t1 - J_t0);

while(cont < t)
    
    centroids = update_centroid(U,matrix,centroids,m);
    
    %Y = update_lambda_matrix(m,matrix,centroids,U);
    %     Y = lambda_matrix(matrix,centroids,U,m);
    Y = lamb(matrix,centroids,U,m);
%     Y(1,1) .* Y(1,2)
%     Y(2,1) .* Y(2,2)
%     Y(3,1) .* Y(3,2)
    
    %U = update_matrix_fuzzy_degree(m, Y, matrix, centroids);
    U = update_matrixU(m, Y, matrix, centroids);
    
    %     sum(U(:,1))
    %     sum(U(:,2))
    %     sum(U(:,3))
    %
    %     U
    
    J_t0 = J_t1;
    J_t1 = kernel_fuzzy(matrix,centroids,U,Y,m);
    
    diff = abs(J_t1 - J_t0)
    if J_t1 < J_t0
        'menor'
    else
        'maior'
    end
    
    if diff <= erro
        break;
    else
    cont = cont+1
    end
end

U = U.';

end

%C�lculo da fun��o objetivo (15). Quanto o menor o valor J, 
%melhor a fun��o de pertinencia.
function [somai] = kernel_fuzzy(matrix, centroids, U,lambda,m)
somai = 0;
somak = 0;
for i = 1:3
    for k = 1:300
       somak = somak + (U(i,k).^m) .* phi_2...
           (lambda(i,:), matrix(k,1:2), centroids(i,:));
    end
    somai = somai + somak;
    somak = 0;
end

end

%Fun��o da atualiza��o da matriz de pesos lambdas
function [Y] = lamb(matrix, centroid, U,m)
Y =zeros(3,2);

sum_numerador_pos11=0;
sum_denominador_pos11=0;
sum_numerador_pos12=0;
sum_denominador_pos12=0;
sum_numerador_pos21=0;
sum_denominador_pos21=0;
sum_numerador_pos22=0;
sum_denominador_pos22=0;
sum_numerador_pos31=0;
sum_denominador_pos31=0;
sum_numerador_pos32=0;
sum_denominador_pos32=0;


for i=1:300
        
    sum_numerador_pos11 =  sum_numerador_pos11 + (U(1,i).^m).* (squared_distance(matrix(i,1),centroid(1,1)));
    sum_denominador_pos11 = sum_denominador_pos11 + (U(1,i).^m).* (squared_distance(matrix(i,1),centroid(1,1)));
     
    sum_numerador_pos12 = sum_numerador_pos12 + (U(1,i).^m).* (squared_distance(matrix(i,2),centroid(1,2)));
    sum_denominador_pos12 = sum_denominador_pos12 + (U(1,i).^m).* (squared_distance(matrix(i,2),centroid(1,2)));
    
    sum_numerador_pos21 = sum_numerador_pos21 + (U(2,i).^m) .* (squared_distance(matrix(i,1), centroid(2,1)));
    sum_denominador_pos21 = sum_denominador_pos21 + (U(2,i).^m) .* (squared_distance(matrix(i,1), centroid(2,1)));
    
    sum_numerador_pos22 = sum_numerador_pos22 + (U(2,i).^m) .* (squared_distance(matrix(i,2), centroid(2,2)));
    sum_denominador_pos22 = sum_denominador_pos22 + (U(2,i).^m) .* (squared_distance(matrix(i,2),centroid(2,2)));
    
    sum_numerador_pos31 = sum_numerador_pos31 + (U(3,i).^m).* (squared_distance(matrix(i,1), centroid(3,1)));
    sum_denominador_pos31 = sum_denominador_pos31 + (U(3,i).^m) .* (squared_distance(matrix(i,1), centroid(3,1)));
    
    sum_numerador_pos32 = sum_numerador_pos32 + (U(3,i).^m).* (squared_distance(matrix(i,2), centroid(3,2)));
    sum_denominador_pos32 = sum_denominador_pos32 + (U(3,i).^m) .* (squared_distance(matrix(i,2), centroid(3,2)));
    
end

produtorio1 = (sum_numerador_pos11 * sum_numerador_pos12) .^ (1/2);
produtorio2 = (sum_numerador_pos21 * sum_numerador_pos22) .^ (1/2);
produtorio3 = (sum_numerador_pos31 * sum_numerador_pos32) .^ (1/2);


Y(1, 1:2) = [(produtorio1/sum_denominador_pos11) (produtorio1/ sum_denominador_pos12)];
Y(2, 1:2) = [(produtorio2/sum_denominador_pos21) (produtorio2/ sum_denominador_pos22)];
Y(3, 1:2) = [(produtorio3/sum_denominador_pos31) (produtorio3/ sum_denominador_pos32)];
            
end


%Fun��o de c�lculo da distancia quadrada entre dois pontos
%eq(16) do artigo
function output = phi_2(pontolambda, x, centroid)

output = 0;
for j = 1:2
    output = output + squared_distance(x(j),centroid(j));
   
%     distance = distance + gaussianKernelComponente(x(j),x(j),i);
%     distance = distance - 2*(gaussianKernelComponente(x(j), ...
%         centroid(j),i));
%     distance = distance + gaussianKernelComponente(centroid(j), ...
%         centroid(j),i);
    output = output * pontolambda(j);
end

end

function [distance] = squared_distance(x,centroid)
%substituir por j
gaussKernel = gaussianKernelComponente(x, ...
        centroid);
distance = (2 - (2*gaussKernel));
    
end


% function [lambda] = create_lambda_matrix()
% 
% lambda = zeros(3,2);
% 
% for i = 1:3
%  lambda(i,1) = rand(1, 1);
%  lambda(i,2) = 1/lambda(i,1);
% end
% 
% end

% Atualiza��o dos centroides
function [centroids] = update_centroid(U,matrix,centroids,m)

% parametro m da quest�o
%m = 2;

numerador = 0;
denominador = 0;

for i = 1:100   
   numerador = numerador + (U(1,i).^m).*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(1,:)) .* matrix(i,1:2); 
   denominador = denominador + U(1,i).^m.*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(1,:));
end

centroids(1,:) = numerador./denominador;

numerador = 0;
denominador = 0;

for i = 101:200   
   numerador = numerador + (U(2,i).^m).*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(2,:)) .* matrix(i,1:2); 
   denominador = denominador + U(2,i).^m.*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(2,:));
end

centroids(2,:) = numerador./denominador;

numerador = 0;
denominador = 0;

for i = 201:300   
   numerador = numerador + (U(3,i).^m).*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(3,:)) .* matrix(i,1:2); 
   denominador = denominador + U(3,i).^m.*gaussianKernelComponente(matrix(i,1:2), ...
       centroids(3,:));
end

centroids(3,:) = numerador./denominador;

end


%Fun��o de atualiza��o da matriz de pertinencia fuzzy
function [U] = update_matrixU(m, lambda, matrix, centroid)
U = zeros(3,300);
for i = 1:3
    for k = 1:300
        U(i,k) = sum_Ufactor(matrix,lambda,centroid,m,i,k).^-1;
    end
end
end

% function [ratio] = sum_Ufactor(matrix,lambda,centroid,m,i,k)
% 
% numerador = 0;
% denominador = 0;
% ratio = 0;
% 
% for h = 1:3
%     numerador = numerador + squared_distance_between_twoPoints...
%         (lambda(i,:), matrix(k,1:2), centroid(i,:), i);
%     denominador = denominador + squared_distance_between_twoPoints...
%         (lambda(h,:), matrix(k,1:2), centroid(h,:), h);
%     ratio = ratio + (numerador/denominador).^ 1/(1-m);
% end
% 
% end

%Fun��o auxiliar para o c�lculo da matriz de pertinencia
function [ratio] = sum_Ufactor(matrix,lambda,centroid,m,i,k)

numerador = 0;
denominador = 0;
ratio = 0;

% numerador = calculate_numerator_Ufactor(matrix,lambda,...
%     centroid,i,k);

for h = 1:3
    numerador = calculate_numerator_Ufactor(matrix,lambda,...
    centroid,i,k);
    denominador = calculate_denominador_Ufactor(matrix,lambda,centroid,h,k);
    ratio = ratio + ((numerador/denominador).^ 1/(m-1));
end

end

%Fun��o auxiliar para o c�lculo da matriz de pertinencia
function [numerador] = calculate_numerator_Ufactor(matrix,lambda,centroid,i,k)
numerador = phi_2...
        (lambda(i,:), matrix(k,1:2), centroid(i,:));
end

%Fun��o auxiliar para o c�lculo da matriz de pertinencia
function [denominador] = calculate_denominador_Ufactor(matrix,lambda,...
    centroid,h,k)
denominador = phi_2...
    (lambda(h,:), matrix(k,1:2), centroid(h,:));
end

%Fun��o que gera a Matriz de pertinencia inicial
function [u_matrix] = generate_MatrixU()
dist = load_distribution();

%n�mero de linhas da matriz
[clusters, patterns] = size(dist.');

u_matrix = zeros(clusters, patterns);
u_matrix = double(u_matrix);

for k = 1 : patterns
    random_matrix = get_randoms();
    u_matrix(1:clusters,k) = random_matrix(1:3);
end

end

%Fun��o que gera um vetor[3] com n�meros aleat�rios para 
%criar a matriz U
function [random_matrix] = get_randoms()
firstRandom = rand(1, 1);

while(firstRandom == 0)
    firstRandom = rand(1, 1);
end

sub = 1 - firstRandom;

secondRandom = rand(1,1);

while(secondRandom == 0)
   secondRandom = rand(1,1);
end

while(secondRandom > sub && secondRandom + firstRandom > 1)
    secondRandom = rand(1,1);
end

    thirdRandom = 1 - (firstRandom + secondRandom);
    random_matrix = [firstRandom secondRandom thirdRandom];
end


%Fun��o que retorna a fun��o Kernel Gaussiana
function kernel = gaussianKernelComponente(pontoJ, centroidJ)

%kernel = exp(-((pontoJ - centroidJ).^2 / (2 .* 1)));

kernel = gaussmf(pontoJ,[1 centroidJ]);

% switch(op)
%     case 1 
%         kernel = exp((pontoJ - centroidJ).^2 / (2 .* 4));
%     case 2
%         kernel = exp((pontoJ - centroidJ).^2 / (2 .* 1));
%     case 3
%         kernel = exp((pontoJ - centroidJ).^2 / (2 .* 0.25));
% end

end

%Fun��o que carrega a distribui��o
function [matrix] = load_distribution()

file = load('amostras.mat');
matrix = struct2array(file);
%dist = trans(matrix);

end

%Fun��o que calcula o centroide inicial das amostras
function [centroids] = find_initial_centroids()

dist = load_distribution();

centroids = zeros(3,2);

% r = randi(100,1,5);
% centroids(1,1) = mean(dist(r,1));
% centroids(1,2) = mean(dist(r,2));
% 
% r = 100 + randi(100,1,5);
% centroids(2,1) = mean(dist(r,1));
% centroids(2,2) = mean(dist(r,2));
% 
% r = 200 + randi(100,1,5);
% centroids(3,1) = mean(dist(r,1));
% centroids(3,2) = mean(dist(r,2));

centroids(1,1) = dist(1,1);
centroids(1,2) = dist(1,2);
centroids(2,1) = dist(101,1);
centroids(2,2) = dist(101,2);
centroids(3,1) = dist(201,1);
centroids(3,2) = dist(201,2);


end

function [B] = trans(A)
[r c] = size(A);
B = zeros(c,r);
for i = 1:r
    for j = 1:c
        B(j,i) = A(i,j);
    end
end
end
