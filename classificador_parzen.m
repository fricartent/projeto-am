% treinamento = conjunto de treinamento
% teste       = conjunto de testes
% h           = matriz das janelas. Cada linha i representa o h para a classe i,
% div         = posicoes que dividem as classes no conjunto de treinamento. 
%               Cada coluna representa a posicao no conjunto de treinamento 
%               onde as amostras da classe terminam. Ex:[200 300]
% kernel      = indica qual a fun��o kernel que ser� utilizada
% classificacao [n,c + 2]
% onde n = quantidade de amostras de teste
%      c = quantidade de classes + 1 coluna para o resultado da
%      classifica��o + 1 coluna para o valor que deveria ser classificado
function classificacao = classificador_parzen(treinamento,teste, h, div, kernel, priori)

%numero de amostras no conjunto de teste
nt = size(teste,1);
%numero de caracteristicas em uma amostra
p = size(teste,2) - 1;

% numero de classes
c = size(h,2);
%c = 2;
classificacao = zeros(nt,c + 2);
classificacao(1:nt,c+ 2) = teste(1:nt, p + 1);
fdp = zeros(nt,c);
for i = 1:nt
    for j = 1:c
        if(j == 1)
            fdp(i,j) = parzen(treinamento(1:div(1,j),1:p),kernel,h(j,1:p),teste(i,1:p));
        else
            fdp(i,j) = parzen(treinamento(div(1,j-1)+1:div(1,j),1:p),kernel,h(j,1:p),teste(i,1:p));
        end
    end
    classe = 1;
    
    for j = 1:c
       classificacao(i,j) = (fdp(i,j) * priori(1,j))/ sum(fdp(i,:) .* priori);
       if classificacao(i,classe) < classificacao(i,j)
           classe = j;
       end
    end
    classificacao(i,c + 1) = classe;
end